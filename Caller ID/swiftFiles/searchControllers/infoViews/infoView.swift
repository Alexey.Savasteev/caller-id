//
//  infoView.swift
//  Caller ID
//
//  Created by User 45 on 12/13/17.
//  Copyright © 2017 User 45. All rights reserved.
//

import UIKit

class infoView: UIView {
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var moreInfo: UIButton!
}
