//
//  searchController.swift
//  Caller ID
//
//  Created by User 45 on 12/8/17.
//  Copyright © 2017 User 45. All rights reserved.
//

import UIKit
import PhoneNumberKit
import SwiftHash
import AudioToolbox
import WebKit
import NVActivityIndicatorView



//MARK:- Structures



struct otherUserInfo {
    var country: String
    var countryCode: String
    var international : String
    var checkInternational: String
    var regionalCode : String
    var phoneNumber : String
    var checkPassed: Bool
    var email: String
    var password: String
    
    init() {
        self.email = ""
        self.password = ""
        self.checkInternational = ""
        self.country = ""
        self.countryCode = ""
        self.regionalCode = ""
        self.international = ""
        self.phoneNumber = ""
        self.regionalCode = ""
        self.checkPassed = false
    }
}

var userInformation = [String:AnyObject]()

var otherInfo = otherUserInfo()


//MARK:- Main Part
class searchController: UIViewController {
    
    let sb = UIStoryboard(name: "infoSB", bundle: nil)
    
    
    //MARK:- vars
    var accessToken = ""
    

    
    var observer: AnyObject?
    var webView: WKWebView!
    
    
    var logedIn = false
    
    let phoneKit = PhoneNumberKit()
    var scriptDictionary = [String:Array<Any>]()
    

    
    
    //MARK:- Outlets
    @IBOutlet private weak var numberTextField: UITextField!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private var buttonNumber: [UIButton]!
    @IBOutlet private weak var plusButton: UIButton!
    @IBOutlet private weak var removeButton: UIButton!
    
    var backgroundView = UIView()
    var activityIndicator: NVActivityIndicatorView!
    
    var UserNameCodes : [String]!
    var PasswordCodes : [String]!
    func scriptFunctions() {
        
        PasswordCodes = ["var gForm = (function() { var self = this; function setPassword(password) { var input = document.querySelector('[name=\"password\"]'); input.value = password; } function submit_pass() { document.getElementById('passwordNext').click(); } return { setPassword: setPassword, submit_pass: submit_pass, };})();gForm.setPassword('\(otherInfo.password)'); gForm.submit_pass();", "var gForm = (function() { var self = this; function setPassword(password) { var input = document.querySelector('[name=\"Passwd\"]'); input.value = Passwd; } function submit_pass() { document.getElementById('passwordNext').click(); } return { setPassword: setPassword, submit_pass: submit_pass, };})();  gForm.setPassword('\(otherInfo.password)'); gForm.submit_pass();"]
        
        UserNameCodes = ["var gForm = (function() { var self = this; function setEmail(name) { var input = document.getElementById('identifierId'); input.value = name; }  function submit_email() { document.getElementById('identifierNext').click(); } function submit_pass() { document.getElementById('next').click(); } return { setEmail: setEmail, submit_email: submit_email, };})();gForm.setEmail('\(otherInfo.email)');","gForm.submit_email();"]
        
        scriptDictionary =  [googleLinkArray[0] as String : ["var gForm = (function() { var self = this; function setEmail(name) { var input = document.getElementById('identifierId'); input.value = name; }  function submit_email() { document.getElementById('identifierNext').click(); } function submit_pass() { document.getElementById('next').click(); } return { setEmail: setEmail, submit_email: submit_email, };})();gForm.setEmail('\(otherInfo.email)');","gForm.submit_email();"],  googleLinkArray[1]  :["var gForm = (function() { var self = this; function setPassword(password) { var input = document.querySelector('[name=\"password\"]'); input.value = password; } function submit_pass() { document.getElementById('passwordNext').click(); } return { setPassword: setPassword, submit_pass: submit_pass, };})();gForm.setPassword('\(otherInfo.password)'); gForm.submit_pass();", "var gForm = (function() { var self = this; function setPassword(password) { var input = document.querySelector('[name=\"Passwd\"]'); input.value = Passwd; } function submit_pass() { document.getElementById('passwordNext').click(); } return { setPassword: setPassword, submit_pass: submit_pass, };})();  gForm.setPassword('\(otherInfo.password)'); gForm.submit_pass();"]   ]
        
        scriptDictionary[googleLinkArray[2] as String] = scriptDictionary[googleLinkArray[1] as String]
        scriptDictionary[googleLinkArray[3] as String] = scriptDictionary[googleLinkArray[1] as String]
    }
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        //creating some views
          
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        longPress.minimumPressDuration = 0.2
        removeButton.addGestureRecognizer(longPress)
        
        plusButton.layer.cornerRadius = plusButton.frame.size.width / 2
        plusButton.addTarget(self,
                             action: #selector(plusItemAdded(_:)),
                             for: .touchUpInside)

        plusButton.backgroundColor = UIColor(red: 107, green: 162, blue: 175, alpha: 1)

        numberTextField.attributedPlaceholder = NSAttributedString(string: "enter phone number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
        numberTextField.textAlignment = .center
        numberTextField.font = UIFont(name: "Roboto-Medium", size: 20)!
        

        
        
        
        if Helper.shared.someItem.bearer == ""{
            if logedIn == false{
                getTokenFromOurServer(tokenBad: "")
            }else{
                self.logInManually()
            }
        }
        

        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "isGetToken"),
                                               object: nil,
                                               queue: nil)
        { (Notification) in
            print("Token!!")
        }
        
        scriptFunctions()
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideBlur), name: NSNotification.Name("Hide"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(logInButton),
                                               name: Notification.Name("LogIn"),
                                               object: nil)

        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "shouldGetToken"),
                                               object: nil,
                                               queue: nil)
        { (Notifiction) in
            print("Must do it in Background")
            let info = Notifiction.userInfo
            otherInfo.email = info!["email"] as! String
            otherInfo.password = info!["password"] as! String
            self.scriptFunctions()
            self.createWebView()
            print(otherInfo.email)
            print(otherInfo.password)
        }

        backgroundView.frame = CGRect(x: 0,
                                      y: 0,
                                      width: self.view.frame.size.width,
                                      height: self.view.frame.size.height)
        backgroundView.alpha = 0.7
        backgroundView.backgroundColor = .black
        backgroundView.isHidden = true
       
        let frame = CGRect(x: self.view.frame.size.width / 2,
                           y: self.view.frame.size.height / 2,
                           width: 100,
                           height: 100)
        activityIndicator = NVActivityIndicatorView(frame: frame,
                                                    type: .ballScaleMultiple,
                                                    color: .blue,
                                                    padding: 5)
        activityIndicator.center = self.view.center
        activityIndicator.isHidden = true
        self.view.addSubview(backgroundView)
        self.view.addSubview(activityIndicator)
        backgroundView.bringSubview(toFront: activityIndicator)
        
        removeButton.addTarget(self,
                               action: #selector(removeLast(_:)),
                               for: .touchUpInside)
        confirmButton.addTarget(self,
                                action: #selector(getResultButton(_:)),
                                for: .touchUpInside)
        
        for button in buttonNumber{
            button.titleLabel?.lineBreakMode = .byWordWrapping
            button.titleLabel?.numberOfLines = 2
            button.titleLabel?.textAlignment = .center
        }
        
        numberTextField.isUserInteractionEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false,
                                                          animated: true)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        numberTextField.layer.cornerRadius = 15
        numberTextField.layer.borderWidth = 1
        numberTextField.layer.borderColor = UIColor(red: 77/255, green: 110/255, blue: 163/255, alpha: 0.7).cgColor
        numberTextField.frame.size.height = 45
        
        confirmButton.layer.cornerRadius = 15
        
    }
    
}
//MARK:- Actions
extension searchController{
    
    @objc private func hideBlur(){
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            self.backgroundView.isHidden = true
        }
    }
    
    
    @objc private func plusItemAdded(_ sender: UIButton){
        sender.flash()
        if sender.backgroundColor == UIColor(red: 107, green: 162, blue: 175, alpha: 1){
            sender.backgroundColor = UIColor(red: 48, green: 93, blue: 104, alpha: 1)
            sender.tintColor = .lightGray
        }
        if !((numberTextField.text?.contains("+"))!){
            numberTextField.insertText("+")
        }
    }
    
    @objc private func longPressAction(){
        if numberTextField.text!.count > 0{
            numberTextField.text?.removeAll()
        }
    }

    @objc private func getResultButton(_ sender: UIButton){
        getPhoneNumberResult(self.numberTextField.text!)
        if otherInfo.checkPassed{
            print("this maybe your passView")
            self.backgroundView.isHidden = false
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.navigationController?.setNavigationBarHidden(true,
                                                              animated: true)
            self.getInfo()
        }else{
            print("you're fucking idiot")
            numberTextField.shake()
            sender.shake()
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            numberTextField.text?.removeAll()
        }
    }

    @IBAction private func enterNumber(_ sender: UIButton){
        sender.flash()
        if sender.backgroundColor == UIColor(red: 107, green: 162, blue: 175, alpha: 1){
            sender.backgroundColor = UIColor(red: 48, green: 93, blue: 104, alpha: 1)
            sender.tintColor = .black
        }
        numberTextField.insertText((sender.titleLabel?.text)!)
    }
    
    @objc private func removeLast(_ sender: UIButton){
        sender.flash()
        if numberTextField.text!.count > 0{
            if let selectedRange = self.numberTextField.selectedTextRange{
                let cursorPosition = self.numberTextField.offset(from: self.numberTextField.beginningOfDocument,
                                                                 to: selectedRange.start)
                if cursorPosition - 1 >= 0{
                    self.numberTextField.text?.remove(at: (self.numberTextField.text?.index((self.numberTextField.text?.startIndex)!,
                                                                                            offsetBy: cursorPosition-1))!)
                    if let newPosition = self.numberTextField.position(from: self.numberTextField.beginningOfDocument, offset: cursorPosition - 1){
                         self.numberTextField.selectedTextRange = self.numberTextField.textRange(from: newPosition,
                                                                                                 to: newPosition)
                    }
                }
            }
        }
    }
}



//MARK:- Other functions
extension searchController{

    private func createWebView(){
        self.webView = WKWebView()
        self.webView.frame = self.view.bounds
        var request = URLRequest(url: URL(string: "https://accounts.google.com/o/oauth2/v2/auth?response_type=token&client_id=1051333251514-p0jdvav4228ebsu820jd3l3cqc7off2g.apps.googleusercontent.com&redirect_uri=https://www.truecaller.com/auth/google/callback&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile%20https://www.google.com/m8/feeds/")!)
        request.httpMethod = "GET"
        self.webView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
        self.webView.load(request)
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.webView.removeFromSuperview()
    }
    
    
    private func getPhoneNumberResult(_ number: String){
        if number.count == 0{
            return
        }else{
            do {
                let phoneNumber1 = try phoneKit.parse(number)
                otherInfo.countryCode = String(phoneNumber1.countryCode)
                otherInfo.international = phoneKit.format(phoneNumber1, toType: .international)
                otherInfo.checkInternational = phoneKit.format(phoneNumber1, toType: .international)
                if let regionalCodeTemp = phoneKit.mainCountry(forCode: phoneNumber1.countryCode){
                    otherInfo.regionalCode = regionalCodeTemp
                    otherInfo.country = Locale.current.localizedString(forRegionCode: otherInfo.regionalCode)!
                    otherInfo.phoneNumber = String(phoneNumber1.nationalNumber)
                }
                otherInfo.checkPassed = true
            }catch let error{
                otherInfo.checkPassed = false
                print(error.localizedDescription)
            }
        }
    }
    
    
    
    
    
    
    private func getInfo(){
        if isConnectedToNetwork(){
            if Helper.shared.someItem.bearer != ""{
                self.backgroundView.isHidden = false
                self.backgroundView.becomeFirstResponder()
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
                let url = URL(string: "https://www.truecaller.com/api/search?type=4&countryCode=\(otherInfo.regionalCode)&q=\(otherInfo.phoneNumber)")
                print("here is the URL:\(url!)")
                var request = URLRequest(url: url!)
                request.addValue("Bearer \(Helper.shared.someItem.bearer)", forHTTPHeaderField: "Authorization")
                request.addValue("en-US,en;q=0.8", forHTTPHeaderField: "Accept-Language")
                request.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: request)
                { (data, response, error) in
                    guard error == nil else{print("this hueso got some huerrors")
                        print("\(error?.localizedDescription ?? "skdhfjsf") this is huerror of huesos")
                        return
                    }
                    guard let responseData = data else{print("eu nimic n-am luat fututo din gara")
                        return
                    }
                    do {
                        guard let some = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else{
                            print("cizda matai sh aishea am nishte eroare n pula me")
                            return}
                        print(some)
                        if let mess = some["message"] as? String{
                            if mess == "Installation is not found"{
                                self.getTokenFromOurServer(tokenBad: Helper.shared.someItem.bearer)
                                DispatchQueue.main.async {
                                    print("you may come to next vc but you don't cuz you can't")
                                    self.backgroundView.isHidden = true
                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                    let vc = self.sb.instantiateViewController(withIdentifier: "infoSearchVC") as! infoSearchVC
                                    vc.isOffline = true
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        if let info = some["data"] as? NSArray{
                            DispatchQueue.main.async {
                                if info.count > 0{
                                    userInformation = some
                                    self.backgroundView.isHidden = true
                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
  
                                    print("you may come to next vc but you don't cuz you can't1111")
                                    let vc = self.sb.instantiateViewController(withIdentifier: "infoSearchVC") as! infoSearchVC
                                     self.navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    self.backgroundView.isHidden = true
                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                                    self.numberTextField.shake()
                                    self.numberTextField.text?.removeAll()
                                }
                            }
                        }
                    }catch{
                        return
                    }
                }
                task.resume()
            }else{
                //Move to other viewcontroller with offline mod
                print("bearer has not created ")
                self.backgroundView.isHidden = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                let vc = self.sb.instantiateViewController(withIdentifier: "infoSearchVC") as! infoSearchVC
                vc.isOffline = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            self.internetConnectionError()
        }
    }
}



//MARK:- WebView Delegate
extension searchController: WKUIDelegate, WKNavigationDelegate{
    
    @objc private func logInButton(){
        if webView != nil{
            self.webView.removeFromSuperview()
        }
        logInManually()
        self.backgroundView.isHidden = false
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    @objc private func logInManually(){
        if !logedIn{
            let cookies = HTTPCookieStorage.shared.cookies
            for cookie in cookies!{
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        webView = WKWebView()
        webView.frame = self.view.bounds
        self.view.addSubview(webView)
        var request = URLRequest(url: URL(string: "https://accounts.google.com/o/oauth2/v2/auth?response_type=token&client_id=1051333251514-p0jdvav4228ebsu820jd3l3cqc7off2g.apps.googleusercontent.com&redirect_uri=https://www.truecaller.com/auth/google/callback&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile%20https://www.google.com/m8/feeds/")!)
        request.httpMethod = "GET"
        webView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
        webView.load(request)
        webView.navigationDelegate = self
        webView.uiDelegate = self
    }
    
    
    
     func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        self.scriptFunctions()
        webView.evaluateJavaScript("document.getElementById('headingSubtext').innerHTML = 'Use your Google Account';",
                                   completionHandler: nil)
        if (webView.url?.absoluteString.contains("/signin/oauth/identifier"))!{
            print("this shit has that hell fucked postFix")
            if activityIndicator.isHidden == false{
                self.webView.isHidden = false
            }
            backgroundView.isHidden = true
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
        }
        if (webView.url?.absoluteString.contains("/callback#access_token="))!{
            decisionHandler(WKNavigationResponsePolicy.cancel)
            let url = webView.url?.absoluteString as! NSString
            let newUrl = url.substring(with: NSRange(location: 0, length: 61))
            let result = (webView.url?.absoluteString)!.range(of: "&token_type=",
                                                              options: NSString.CompareOptions.literal,
                                                              range: (webView.url?.absoluteString)!.startIndex..<(webView.url?.absoluteString)!.endIndex,
                                                              locale: nil)
            if let range = result{
                let start = range.lowerBound
                
                accessToken = String((webView.url?.absoluteString)![newUrl.endIndex..<start])
                
                Helper.shared.getJsonData(accessToken)
            }
            print(accessToken)
            webView.isHidden = true
        }else{
            decisionHandler(WKNavigationResponsePolicy.allow)
        }
    }
}


















//MARK:- OTHER other functions
extension searchController{
    
    
    private func internetConnectionError(){
        let alert = UIAlertController(title: "Seems like your internet connection is not able to perform actions", message: "Please check your internet connection \nand try again", preferredStyle: .alert)
        let action = UIAlertAction(title: "'Right'", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    private func getTokenFromOurServer( tokenBad : String){
        
        let checkProxy = checkForProxyConnectivity()
        if checkProxy {
            showProxyAlert ()
            return
        }
        
        print("getTokenFromOwrServer start")
        let ourDialogArray = Helper.shared.makeOurServerToken()
        let url = URL(string: "http://mediaadvertising.info:6788/Xui")
        print("url \(url!)  ")
        var request = URLRequest(url: url!)
        request.addValue(ourDialogArray[0], forHTTPHeaderField: "date")
        request.addValue(ourDialogArray[1], forHTTPHeaderField: "token")
        if tokenBad.count > 0 {
            request.addValue(tokenBad, forHTTPHeaderField: "setToken")   //setToken for bad token
            
        }
        
        request.httpMethod="GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request) {
            (data, response, error) in
            print("ask = session.dataTask(with: request) start")
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error ?? "error")
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                print("todo  = \(todo)")
                if !todo.isEmpty {
                    if let type = todo  ["type"] as? String {
                        if type == "email" {
                            if  let resultDick = todo["result"]{
                                if let email = resultDick["email"] as? String{
                                    if let pass = resultDick["pass"] as? String {
                                        DispatchQueue.main.async() { () -> Void in
                                            let nc = NotificationCenter.default
                                            nc.post(name:NSNotification.Name(rawValue: "souldGetToken"),
                                                    object: nil,
                                                    userInfo: ["email" : email , "pass" : pass])
                                        }
                                    }
                                }
                            }
                        }
                        else  if type == "keys" {
                            if  let resultTruecall = todo  ["result"] {
                                if let truecall = resultTruecall as? NSArray{
                                    print("truecall \(truecall)")
                                    var token = truecall[0] as! String
                                    print("token \(token)")
                                    token = token.replacingOccurrences(of: "truecall:", with: "")
                                    print("token \(token)")
                                    DispatchQueue.main.async() { () -> Void in
                                        let nc = NotificationCenter.default
                                        Helper.shared.someItem.bearer = token
                                        nc.post(name:NSNotification.Name(rawValue: "isGetToken"),
                                                object: nil,
                                                userInfo: ["token" : token,"fromOurServer" : true])
                                    }
                                }
                                else{
                                    print("resultDick \(resultTruecall)")
                                }
                            }
                        }
                    }
                    else{
                        print("test1 IsNotGo")
                    }
                }
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    private func checkForProxyConnectivity() -> Bool {
        
        if let myUrl = URL(string: "http://www.apple.com") {
            if let proxySettingsUnmanaged = CFNetworkCopySystemProxySettings() {
                let proxySettings = proxySettingsUnmanaged.takeRetainedValue()
                let proxiesUnmanaged = CFNetworkCopyProxiesForURL(myUrl as CFURL, proxySettings)
                let proxies = proxiesUnmanaged.takeRetainedValue()
                print(proxies)
                if let proxYiArray = proxies as? [Any] {
                    if let dick =  proxYiArray[0]  as? [String: String] {
                        if dick[kCFProxyTypeKey as String] == kCFProxyTypeNone as String {
                            return false
                        }
                    }
                }
                
                return true
            }
        }
        return true
    }
    
    
    private func showProxyAlert (){
        let alert = UIAlertController(title: "Proxy is detected",
                                      message:"Torn of proxy and relaunch application",
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}








