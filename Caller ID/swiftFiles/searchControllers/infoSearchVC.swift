//
//  infoSearchVC.swift
//  Caller ID
//
//  Created by User 45 on 12/12/17.
//  Copyright © 2017 User 45. All rights reserved.
//

import UIKit


//MARK:- Main Part
class infoSearchVC: UIViewController {
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    
    public var isOffline = false
    private var userInfo = userInformationData()
    private var userInfoArr = [userInformationData]()
    
    @IBOutlet private weak var infoScrollView: UIScrollView!
    @IBOutlet private weak var infoView1: UIView!
    @IBOutlet private weak var mapButton: UIButton!
    @IBOutlet private weak var avatarImage: UIImageView!
    
    @IBOutlet private weak var numberImage: UIImageView!
    @IBOutlet private weak var numberLabel: UILabel!
    
    @IBOutlet private weak var addressImage: UIImageView!
    @IBOutlet private weak var addressLabel: UILabel!
    
    @IBOutlet private weak var nameLabel: UILabel!
    
    @IBOutlet private weak var tagImage: UIImageView!
    @IBOutlet private weak var tagLabel: UILabel!
    
    
    private var viewsForScroll = [infoView]()
    private var numberType: moreInfo!
    private var carrier: moreInfo!
    private var typePhone: moreInfo!
    
    private var timeZone: moreInfo!
    private var countryCode: moreInfo!
    private var city: moreInfo?
    private var street: moreInfo?
    
    var slide: [infoView] = []
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
        
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        infoScrollView.backgroundColor = UIColor(red: 247/255, green: 246/255, blue: 252/255, alpha: 1)
        infoScrollView.isScrollEnabled = true
        
        assignbackground("Back-1")
        
        if !isOffline{
            userInfo = Helper.shared.getInformationFromDictionaty()
            
            
            
            
            
            numberType = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! moreInfo
            carrier = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! moreInfo
            typePhone = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! moreInfo
            numberType.layer.cornerRadius = 10
            carrier.layer.cornerRadius = 10
            typePhone.layer.cornerRadius = 10
            self.numberType.captionLabel.text = "Number Type:"
            if userInfo.numberType != ""{
                self.numberType.infoLabel.text = self.userInfo.numberType
            }else{
                self.numberType.infoLabel.text = "NO INFO"
            }
            carrier.captionLabel.text = "Carrier:"
            if userInfo.carrier != ""{
                carrier.infoLabel.text = self.userInfo.carrier
            }else{
                carrier.infoLabel.text = "NO INFO"
            }
            typePhone.captionLabel.text = "Type:"
            if userInfo.type != ""{
                typePhone.infoLabel.text = self.userInfo.type
            }else{
                typePhone.infoLabel.text = "NO INFO"
            }
            numberType.frame = CGRect(x: self.view.frame.width / 4, y: 40, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            carrier.frame = CGRect(x: self.view.frame.width / 4, y: 40, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            typePhone.frame = CGRect(x: self.view.frame.width / 4, y: 40, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            numberType.alpha = 0
            carrier.alpha = 0
            typePhone.alpha = 0
            self.infoScrollView.addSubview(numberType)
            self.infoScrollView.addSubview(carrier)
            self.infoScrollView.addSubview(typePhone)
            
            
            timeZone = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! moreInfo
            countryCode = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! moreInfo
            city = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? moreInfo
            street = UINib(nibName: "moreInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? moreInfo
            timeZone.layer.cornerRadius = 10
            countryCode.layer.cornerRadius = 10
            city?.layer.cornerRadius = 10
            street?.layer.cornerRadius = 10
            timeZone.captionLabel.text = "Time Zone:"
            countryCode.captionLabel.text = "Country Code:"
            city?.captionLabel.text = "City:"
            street?.captionLabel.text = "Street:"
            if userInfo.timeZone != ""{
                timeZone.infoLabel.text = self.userInfo.timeZone
            }else{
                timeZone.infoLabel.text = "NO INFO"
            }
            if userInfo.countryCode != ""{
                countryCode.infoLabel.text = self.userInfo.countryCode
            }else{
                countryCode.infoLabel.text = "NO INFO"
            }
            if userInfo.city != ""{
                city?.infoLabel.text = self.userInfo.city
            }else{
                city?.infoLabel.text = "NO INFO"
            }
            if userInfo.street != ""{
                street?.infoLabel.text = self.userInfo.street
            }else{
                street?.infoLabel.text = "NO INFO"
            }
            timeZone.frame = CGRect(x: self.view.frame.width / 4, y: 150, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            countryCode.frame = CGRect(x: self.view.frame.width / 4, y: 150, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            city?.frame = CGRect(x: self.view.frame.width / 4, y: 150, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            street?.frame = CGRect(x: self.view.frame.width / 4, y: 150, width: self.view.frame.width - self.view.frame.width / 4, height: 45)
            timeZone.alpha = 0
            countryCode.alpha = 0
            city?.alpha = 0
            street?.alpha = 0
            infoScrollView.addSubview(timeZone)
            infoScrollView.addSubview(countryCode)
            infoScrollView.addSubview(city!)
            infoScrollView.addSubview(street!)
            
            slide = createSlides()
            setupSlides(slide, isDidLoad: true)
            setupSlides(slide, isDidLoad: false)
        }else{
            print("you have to Log In ")
            let customView = UINib(nibName: "logInView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! logInView
            customView.logInButton.addTarget(self, action: #selector(logIn), for: .touchUpInside)
            customView.center = CGPoint(x: self.view.frame.width / 2, y: 150)
            infoScrollView.addSubview(customView)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc private func logIn(){
        NotificationCenter.default.post(name: Notification.Name("LogIn"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func assignbackground(_ name: String){
        let background = UIImage(named: name)
        var imageView : UIImageView!
        imageView = UIImageView(frame: infoView1.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = infoView1.center
        infoView1.addSubview(imageView)
        self.infoView1.sendSubview(toBack: imageView)
    }
    
}



//MARK:- Actions
extension infoSearchVC{
    
    @objc private func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc private func showMoreInfo(_ sender: UIButton){
        let view1 = sender.superview as! infoView
        if view1.infoTitle.text == "Phone Number"{
            UIView.animate(withDuration: 0.3, animations: {
                if sender.transform == .identity {
                    self.infoScrollView.contentSize.height += 150
                    sender.transform = CGAffineTransform(rotationAngle: .pi)
                    self.numberType.frame.origin = CGPoint(x: self.numberType.frame.origin.x, y: self.numberType.frame.origin.y + 33)
                    self.numberType.alpha = 1
                    self.carrier.frame.origin = CGPoint(x: self.carrier.frame.origin.x, y: self.carrier.frame.origin.y + 83)
                    self.carrier.alpha = 1
                    self.typePhone.frame.origin = CGPoint(x: self.typePhone.frame.origin.x, y: self.typePhone.frame.origin.y + 133)
                    self.typePhone.alpha = 1
                    for some in self.viewsForScroll{
                        if some != view1{
                            some.frame.origin = CGPoint(x: some.frame.origin.x,
                                                        y: some.frame.origin.y + 150)
                        }
                    }
                    self.countryCode.frame.origin = CGPoint(x: self.countryCode.frame.origin.x, y: self.countryCode.frame.origin.y + 130)
                    if self.city! != nil {
                        self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y + 130)
                    }
                    if self.street! != nil{
                        self.street!.frame.origin = CGPoint(x: self.street!.frame.origin.x, y: self.street!.frame.origin.y + 130)
                    }
                    self.timeZone.frame.origin = CGPoint(x: self.timeZone.frame.origin.x, y: self.timeZone.frame.origin.y + 130)
                }else{
                    sender.transform = .identity
                    self.infoScrollView.contentSize.height -= 150
                    self.numberType.frame.origin = CGPoint(x: self.numberType.frame.origin.x, y: self.numberType.frame.origin.y - 33)
                    self.numberType.alpha = 0
                    self.carrier.frame.origin = CGPoint(x: self.carrier.frame.origin.x, y: self.carrier.frame.origin.y - 83)
                    self.carrier.alpha = 0
                    self.typePhone.frame.origin = CGPoint(x: self.typePhone.frame.origin.x, y: self.typePhone.frame.origin.y - 133)
                    self.typePhone.alpha = 0
                    for some in self.viewsForScroll{
                        if some != view1{
                            some.frame.origin = CGPoint(x: some.frame.origin.x,
                                                        y: some.frame.origin.y - 150)
                        }
                    }
                    self.countryCode.frame.origin = CGPoint(x: self.countryCode.frame.origin.x, y: self.countryCode.frame.origin.y - 130)
                    if self.city! != nil{
                        self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y - 130)
                    }
                    if self.street! != nil{
                        self.street!.frame.origin = CGPoint(x: self.street!.frame.origin.x, y: self.street!.frame.origin.y - 130)
                    }
                    self.timeZone.frame.origin = CGPoint(x: self.timeZone.frame.origin.x, y: self.timeZone.frame.origin.y - 130)
                }
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                if sender.transform == .identity{
                    if self.city! != nil && self.street! != nil{
                        self.infoScrollView.contentSize.height += 200
                    }
                    sender.transform = CGAffineTransform(rotationAngle: .pi)
                    self.countryCode.frame.origin = CGPoint(x: self.countryCode.frame.origin.x, y: self.countryCode.frame.origin.y + 25)
                    self.countryCode.alpha = 1
                    if self.city != nil{
                        if self.street != nil{
                            self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y + 175)
                        }else{
                            self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y + 120)
                        }
                        self.city!.alpha = 1
                    }
                    if self.street != nil{
                        self.street!.frame.origin = CGPoint(x: self.street!.frame.origin.x, y: self.street!.frame.origin.y + 125)
                        self.street!.alpha = 1
                    }
                    self.timeZone.frame.origin = CGPoint(x: self.timeZone.frame.origin.x, y: self.timeZone.frame.origin.y + 75)
                    self.timeZone.alpha = 1
                    for some in self.viewsForScroll{
                        if some != view1 && some != self.viewsForScroll[0]{
                            if self.city != nil && self.street != nil{
                                some.frame.origin = CGPoint(x: some.frame.origin.x, y: some.frame.origin.y + 200)
                            }
                        }
                    }
                }else{
                    sender.transform = .identity
                    self.countryCode.frame.origin = CGPoint(x: self.countryCode.frame.origin.x, y: self.countryCode.frame.origin.y - 25)
                    self.countryCode.alpha = 0
                    if self.city != nil{
                        if self.street != nil{
                            self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y - 175)
                        }else{
                            self.city!.frame.origin = CGPoint(x: self.city!.frame.origin.x, y: self.city!.frame.origin.y - 120)
                        }
                        self.city!.alpha = 0
                    }
                    if self.street != nil{
                        self.street!.frame.origin = CGPoint(x: self.street!.frame.origin.x, y: self.street!.frame.origin.y - 125)
                        self.street!.alpha = 0
                    }
                    self.timeZone.frame.origin = CGPoint(x: self.timeZone.frame.origin.x, y: self.timeZone.frame.origin.y - 75)
                    self.timeZone.alpha = 0
                    for some in self.viewsForScroll{
                        if some != view1 && some != self.viewsForScroll[0]{
                            if self.city != nil && self.street != nil {
                                some.frame.origin = CGPoint(x: some.frame.origin.x, y: some.frame.origin.y - 200)
                            }
                        }
                    }
                        if self.city != nil && self.street != nil{
                            self.infoScrollView.contentSize.height -= 200
                        }
                    }
                })
            }
        }
    }



    //MARK:- Slide Creating
    extension infoSearchVC{
        
        private func createSlides() -> [infoView]{
            var slideArray = [infoView]()
            if userInfo.phoneNumber != ""{
                let numberView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                numberView.layer.cornerRadius = 10
                numberView.infoTitle.text = "Phone Number"
                numberView.infoLabel.text = userInfo.phoneNumber
                numberView.moreInfo.isHidden = false
                numberView.moreInfo.addTarget(self, action: #selector(showMoreInfo(_:)), for: .touchUpInside)
                numberView.infoImage.image = UIImage(named: "phone")
                slideArray.append(numberView)
                self.viewsForScroll.append(numberView)
            }
            if userInfo.address != ""{
                let addressView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                addressView.layer.cornerRadius = 10
                addressView.infoTitle.text = "Address"
                addressView.infoLabel.text = userInfo.address
                addressView.moreInfo.isHidden = false
                addressView.moreInfo.addTarget(self, action: #selector(showMoreInfo(_:)), for: .touchUpInside)
                addressView.infoImage.image = UIImage(named: "point")
                slideArray.append(addressView)
                self.viewsForScroll.append(addressView)
                
            }
            if userInfo.email != ""{
                let emailView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                emailView.layer.cornerRadius = 10
                emailView.infoTitle.text = "Email"
                emailView.infoLabel.text = userInfo.email
                emailView.moreInfo.isHidden = true
                emailView.infoImage.image = UIImage(named: "email")
                slideArray.append(emailView)
                self.viewsForScroll.append(emailView)
                
            }
            if userInfo.fbID != ""{
                let facebookView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                facebookView.layer.cornerRadius = 10
                facebookView.infoTitle.text = "Facebook ID"
                facebookView.infoLabel.text = userInfo.fbID
                facebookView.moreInfo.isHidden = true
                facebookView.infoImage.image = UIImage(named: "fb")
                slideArray.append(facebookView)
                self.viewsForScroll.append(facebookView)
                
            }
            if userInfo.link != ""{
                let linkView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                linkView.layer.cornerRadius = 10
                linkView.infoTitle.text = "Link"
                linkView.infoLabel.text = userInfo.link
                linkView.moreInfo.isHidden = true
                linkView.infoImage.image = UIImage(named: "link")
                slideArray.append(linkView)
                self.viewsForScroll.append(linkView)
                
            }
            if userInfo.gender != ""{
                let genderView: infoView = Bundle.main.loadNibNamed("infoView", owner: self, options: nil)?.first as! infoView
                genderView.layer.cornerRadius = 10
                genderView.infoTitle.text = "Gender"
                genderView.infoLabel.text = userInfo.gender
                genderView.moreInfo.isHidden = true
                genderView.infoImage.image = UIImage(named: "gender")
                slideArray.append(genderView)
                self.viewsForScroll.append(genderView)
                
            }
            return slideArray
        }
        
        
        func setupSlides(_ slide: [infoView], isDidLoad: Bool){
            
            for i in 0..<slide.count{
                if isDidLoad{
                    infoScrollView.addSubview(slide[i])
                }else{
                    slide[i].frame = CGRect(x: 0, y: CGFloat(i) * 85, width: self.infoScrollView.frame.width, height: 80)
                    infoScrollView.contentSize.height = 85 * CGFloat(slide.count)
                }
            }
            
        }
}


