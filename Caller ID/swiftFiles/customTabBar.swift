//
//  customTabBar.swift
//  Caller ID
//
//  Created by User 45 on 12/15/17.
//  Copyright © 2017 User 45. All rights reserved.
//

import UIKit

class customTabBar: UITabBarController {

    let sb = UIStoryboard(name: "mainScene", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor = UIColor(red: 77/255, green: 110/255, blue: 163, alpha: 1)
        
        
        
        let searchVC = sb.instantiateViewController(withIdentifier: "searchController")
        searchVC.tabBarItem.selectedImage = UIImage(named: "searchActive")?.withRenderingMode(.alwaysOriginal)
        searchVC.tabBarItem.image = UIImage(named: "searchUnactive")?.withRenderingMode(.alwaysOriginal)
        searchVC.tabBarItem.title = nil
        searchVC.tabBarItem.tag = 1
        let searchNav = UINavigationController(rootViewController: searchVC)
        
        
        
        
        viewControllers = [searchNav]
        
        
        
    }


}








final class MainTabBar: UITabBar {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // iOS 11: puts the titles to the right of image for horizontal size class regular. Only want offset when compact.
        // iOS 9 & 10: always puts titles under the image. Always want offset.
        var verticalOffset: CGFloat = 6.0
        
        if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
            verticalOffset = 0.0
        }
        
        let imageInset = UIEdgeInsets(
            top: verticalOffset,
            left: 0.0,
            bottom: -verticalOffset,
            right: 0.0
        )
        
        for tabBarItem in items ?? [] {
            tabBarItem.title = ""
            tabBarItem.imageInsets = imageInset
        }
    }
}

